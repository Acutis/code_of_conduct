# CODE OF CONDUCT  

## Core Values  

* Follow the Golden Rule  
  - Loce your neighbor as yourself (To love others well, you have to love yourself.  You can't give what you don't have.)  
* Humility  
  - Freedom from pride and arrogance allow us to serve others with joy.  
* Commitment to Continuous Improvement (CCI)  
  - There is always room for improvement as long as we never settle for second best.  
* A Culture of Accountability  
  - Doing what you said you would do, as you said you would do it, when you said you would do it.  
* Fear...Is Only a Distraction  
  - Fear may always be ever-present, do not let it be a dstraction from doing your best work.  
* Team  
  - None of us is as smart or productive as all of us.  
* Servant Leadership  
  - Its's not about how hard you push; its about how well you serve.  
* Focus  
  - Our vision can be blurred and we need to give total committment to the goals at hand and stay on the right track.  

## Our Pledge  

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, sex, disability, ethnicity, experience, nationality, race, or religion.  

## Our Standards  

Examples of behavior that contributes to creating a positive environment include:  

* Be welcoming and helpful to everyone in the community and anyone whom the community supports
* Being respectful of viewpoints and experiences  
* Politey accepting of constructive criticism  
* Focusing on what is best for the community and project  

Examples of unacceptable behavior by participants include:  

* The use of sexualized language or imagery and unwelcome sexual attention or advances  
* Personal and private discussions in this public and community setting  
* Trolling, insulting/derogatory comments, and personal attacks  
* Public or private harassment  
* Publishing others' private information, such as a physical or electronic address regardless of permission  
* Other conduct which could reasonably be considered inappropriate in a professional setting  
